#include "PerishableProduct.h"

PerishableProduct::PerishableProduct(uint32_t id, const std::string name, float rawPrice, const std::string expirationDate):
	Product(id, name, rawPrice), m_expirationDate(expirationDate)
{
}

std::string PerishableProduct::getExpirationDate()
{
	return m_expirationDate;
}

uint32_t PerishableProduct::getVAT()
{
	return 9;
}

float PerishableProduct::getPrice()
{
	return m_rawPrice + m_rawPrice * 0.09;
}
