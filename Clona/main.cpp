#include "Product.h"
#include <fstream>
#include <deque>
#include <algorithm>

int main()
{
	std::deque<Product> products;
	uint16_t id;
	std::string name;
	float price;
	uint16_t vat;
	std::string dateOrType;
	//products.reserve(11);
	for (std::ifstream inputFile("Products.prodb"); !inputFile.eof(); )
	{
		inputFile >> id >> name >> price >> vat >> dateOrType;
		//validate input
		products.emplace_back(id, name, price, vat, dateOrType);
	}
	return 0;
}